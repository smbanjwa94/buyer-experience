---
title: Managing Support contacts and handling details
description: Taking control over who has access to your support entitlement
side_menu:
  links:
  - text: Getting Set Up
    href: "#getting-set-up"
    children:
    - text: Proving your Support Entitlement
      href: "#proving-your-support-entitlement"
  - text: Managing contacts
    href: "#managing-contacts"
    children:
    - text: Authorized contacts
      href: "#authorized-contacts"
  - text: Shared Organizations
    href: "#shared-organizations"
  - text: Special Handling Notes
    href: "#special-handling-notes"
components:
- name: call-to-action
  data:
    title: Managing Support contacts and handling details
    centered_by_default: true
    hide_title_image: true
- name: copy
  data:
    block:
    - hide_horizontal_rule: true
      no_margin_bottom: true
      text: |
        The GitLab support team is here to help. To avoid any delays in processing your tickets we recommend seeding a list of named contacts. If a ticket is submitted by someone not on your list, we'll ask them to [prove their support entitlement](#proving-your-support-entitlement) which will delay our initial response.

        ## Getting Set Up {#getting-set-up}

        Once your license or subscription is provisioned, we recommend submitting an initial ticket with a list of contacts who are allowed to contact Support.

        1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `First time setup` in the Problem type field.
        1. You'll likely receive a reply letting you know that you need to [prove your support entitlement](#proving-your-support-entitlement), so be prepared with those details.

        In the same ticket, you may also want to update ticket visibility by making a [shared organization](#shared-organizations), or lock any organization changes down by specifying a list of [authorized contacts](#authorized-contacts). If there are some [special handling notes](#special-handling-notes) you'd like for us to add, we're happy to do that as well.

        ### Proving your Support Entitlement {#proving-your-support-entitlement}

        Depending on how you purchased GitLab, GitLab Support may not automatically detect your support entitlement on the creation of your first support ticket. If that's the case, you will be asked to provide evidence that you have an active license or subscription.

        #### For GitLab.com Users

        To ensure that we can match you with your GitLab.com subscription when opening a support ticket, please:
        - include your GitLab.com username; AND
        - use the primary email address associated with your account; AND
        - reference a path within a GitLab.com group (that you are a member of or attempting to join), which has a valid subscription associated with it (such as a link to a problematic pipeline or MR)

        #### For Self-managed Users

        To ensure that we can match you with your Self-managed license when opening a support ticket, please:
        - use a company provided email address (no generic email addresses such as Gmail, Yahoo, etc.); AND
        - provide GitLab with one of the following:
           - A screenshot of the license page
              - versions older than 14.1 see `/admin/license`
              - versions 14.1 or newer see `/admin/subscription`
           - The output of the command `sudo gitlab-rake gitlab:license:info`
           - The license ID displayed on the `/admin/license` page (GitLab 13.2+)
           - The license file provided to you at the time of purchase **and at least one of these:**
             - the email address to which the license was issued
             - the company name to which the license was issued

        ## Managing contacts {#managing-contacts}

        Additional Support contacts can be managed by any user who can [prove their support entitlement](#proving-your-support-entitlement) by submitting a request using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and selecting `Manage my organization's contacts` in the Problem type field.

        ### Maximum number of support contacts

        Currently, we limit the maximum number of support contacts to 30 per organization.

        Should a request to add more or setup a shared organization arise when at the limit (or when the request would put you over the limit), the Support Operations team will discuss this with you to find a resolution.

        #### Using an email alias or distribution group as a support contact

        Some organizations prefer to use a generic email address like an alias or distribution group for one of their registered support contacts. This will work, but for the smoothest experience consider the following:
          1. Set a login password for this support user and share it within your team.
          2. When you raise a ticket, always log in: this will allow you to add CCs to any tickets you raise.
          3. CC any email addresses that may be involved in the resolution of the ticket: this will allow other individuals in the organization to reply to the ticket via email.

        ### Authorized contacts {#authorized-contacts}

        For organizations that require additional security, you can specify a set of authorized contacts who can make changes.

        1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Other requests` in the Problem type field.

        We will add an internal note detailing who is allowed to make changes to the contacts in your organization.

        ## Shared Organizations {#shared-organizations}

        In some cases, certain organizations want all members of their organization to be able to see all of the support tickets that have been logged. In other cases, a particular user from the account would like to be able to see and respond to all tickets from their organization. If you'd like to enable this, please:

        **Global Support Shared Organization Setup**
        1. Submit a Support ticket using the Global [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Shared organization requests` in the Problem type field.

        **US Federal Support Shared Organization Setup**
        1. Submit a Support ticket in the US Federal Support portal's [requests for shared organization setup form](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421052) and fill in what type of sharing you would like setup. Kindly note that if you wish to enable shared organizations in both portals you must submit a separate request using the Global Support portal form.

        ## Special Handling Notes {#special-handling-notes}

        If there's a special set of handling instructions you'd like included in your Organizations notes, we'll do our best to comply. Not all requests can be accommodated, but
        if there is something we can do to make your support experience better we want to know.

        1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Other Requests` in the Problem type field.
